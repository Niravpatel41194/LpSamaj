package com.lpsamaj.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LpsamajApplication {

	public static void main(String[] args) {
		SpringApplication.run(LpsamajApplication.class, args);
	}

}
