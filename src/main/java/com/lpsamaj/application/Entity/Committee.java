package com.lpsamaj.application.Entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "tbl_committee")
public class Committee extends Audit{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "committee_id")
    private String id;

    @OneToOne
    @JoinColumn(name = "admin_id")
    private AdminType admin_Type;

    @Column(name = "committee_name")
    private String committee_name;


    @Override
    public String getResourceName() {
        return this.committee_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AdminType getAdmin_Type() {
        return admin_Type;
    }

    public void setAdmin_Type(AdminType admin_Type) {
        this.admin_Type = admin_Type;
    }

    public String getCommittee_name() {
        return committee_name;
    }

    public void setCommittee_name(String committee_name) {
        this.committee_name = committee_name;
    }


}
