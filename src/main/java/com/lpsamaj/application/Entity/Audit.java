package com.lpsamaj.application.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.sql.Timestamp;

@MappedSuperclass()
public abstract class Audit {

    @CreatedDate
    @Column(name = "created_at")
    private Timestamp created_at;

    @OneToOne
    @CreatedBy
    @JoinColumn(name = "created_by")
    private AdminType created_by;

    @LastModifiedDate
    @Column(name = "updated_at")
    private Timestamp updated_at;

    @OneToOne
    @LastModifiedBy
    @JoinColumn(name = "updated_by")
    private AdminType updated_by;

    @JsonIgnore
    public abstract  String getResourceName();

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public AdminType getCreated_by() {
        return created_by;
    }

    public void setCreated_by(AdminType created_by) {
        this.created_by = created_by;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public AdminType getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(AdminType updated_by) {
        this.updated_by = updated_by;
    }

    public void setResourceName(String name){
        //to set resource name of importing entity
    }


}
