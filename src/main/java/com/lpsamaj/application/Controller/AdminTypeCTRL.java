package com.lpsamaj.application.Controller;

import com.lpsamaj.application.Entity.AdminType;
import com.lpsamaj.application.Repository.AdminTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admintype")
public class AdminTypeCTRL {

    @Autowired
    AdminTypeRepo adminTypeRepo;

    @GetMapping("/")
    public List<AdminType> getall(){
        return adminTypeRepo.findAll();
    }

    @PostMapping("/save")
    public AdminType saveAdminType(@RequestBody AdminType adminType){
        return this.adminTypeRepo.save(adminType);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable String id){
        this.adminTypeRepo.deleteById(id);
    }

}
