package com.lpsamaj.application.Controller;

import com.lpsamaj.application.Entity.Activity;
import com.lpsamaj.application.Repository.ActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityCTRL {

    @Autowired
    ActivityRepo activityRepo;

    @GetMapping("/")
    public List<Activity> getAll(){
        return activityRepo.findAll();
    }

    @PostMapping("/save")
    public Activity saveActivity(@RequestBody Activity activity){
        return this.activityRepo.save(activity);
    }
}
