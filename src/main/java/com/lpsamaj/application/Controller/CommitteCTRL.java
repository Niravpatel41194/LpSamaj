package com.lpsamaj.application.Controller;

import com.lpsamaj.application.Entity.Committee;
import com.lpsamaj.application.Repository.CommitteeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/committe")
public class CommitteCTRL {

    @Autowired
    CommitteeRepo committeeRepo;

    @GetMapping("/")
    public List<Committee> getAll(){
        return this.committeeRepo.findAll();
    }

    @PostMapping("/save")
    public Committee saveCommitte(Committee committee){
        return this.committeeRepo.save(committee);
    }
}
