package com.lpsamaj.application.Repository;

import com.lpsamaj.application.Entity.AdminType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminTypeRepo extends JpaRepository<AdminType, String> {
}
