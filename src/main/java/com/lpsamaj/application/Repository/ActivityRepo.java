package com.lpsamaj.application.Repository;

import com.lpsamaj.application.Entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepo extends JpaRepository<Activity, String> {
}
