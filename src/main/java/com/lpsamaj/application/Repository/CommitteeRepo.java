package com.lpsamaj.application.Repository;

import com.lpsamaj.application.Entity.Committee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommitteeRepo extends JpaRepository<Committee,String> {
}
